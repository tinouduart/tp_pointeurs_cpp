#include <iostream>
#include "address.h"

using namespace std;

void showAddress(int x)
{
    cout << &x;
}

void showValue(int &x)
{
    cout << x;
}

void swap(int* p1, int* p2)
{
    int tmp = *p1;
    *p1 = *p2;
    *p2 = tmp;
}

void func(int tab[], int size, int** min, int** max)
{
    *min = &tab[0];
    *max = &tab[0];

    for (int i = 1; i < size; i++)
    {
        if (tab[i] < **min)
            *min = &tab[i];

        if (tab[i] > **max)
            *max = &tab[i];
    }
}

void fillArray(int* tab)
{
    int nb = 0;

    /* Vérification d'usage : tab est-t-il bien définit ? */
    if ( !tab )
        return;

    cout << "entrez 5 nombres : ";
    
    /* On va récupérer 5 fois un nombre sur l'entrée standard et stocker celui-ci dans le tableau */
    for (int i = 0; i < SECOND_ARRAY_SIZE; i++)
    {
        cin >> nb;

        /* Déréférencement du pointeur tab => on lui affecte la valeur récupérée via cin */
        *tab = nb;

        /* On incrémente le pointer (ie: on passe à la case d'après) */
        tab++;
    }
}