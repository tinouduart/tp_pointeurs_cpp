#include <iostream>
#include "address.h"

using namespace std;

int main(int argc, char *argv[]) 
{
	int x = 12;
    int y = 28;

    int* min = nullptr;
    int* max = nullptr;

    int* p1 = nullptr;
    int* p2 = nullptr;

    int tab[FIRST_ARRAY_SIZE] = {10, 5, 5 ,89, 16, 11, 2, 4, 3, 8};

    int tab2[SECOND_ARRAY_SIZE] = { 0 };

    int* p_tab = tab2;

    cout << "adresse : ";
    showAddress(x);
    cout << "\tvaleur : ";
    showValue(x);
    cout << "\n";

    p1 = &x;
    p2 = &y;

    cout << "p1 = " << *p1 << "\t\tp2 = " << *p2 << "\n";
    swap(p1, p2);
    cout << "p1 = " << *p1 << "\t\tp2 = " << *p2 << "\n";

    func(tab, FIRST_ARRAY_SIZE, &min, &max);
    cout << "min : " << *min << "\t\tmax : " << *max << "\n";

    fillArray(tab2);

    for (int i = 0; i < SECOND_ARRAY_SIZE; i++)
        cout << *p_tab++ << " ";

    cout << endl;

    return EXIT_SUCCESS;
}