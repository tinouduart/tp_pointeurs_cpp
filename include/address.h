#ifndef ADDRESS_H
#define ADDRESS_H

#define FIRST_ARRAY_SIZE 10
#define SECOND_ARRAY_SIZE 5

/* void showAddress(int x);
 * Permet d'afficher l'adresse de la variable x.
 * 
 * int x : Variable dont on cherche à afficher l'adresse.
 */
void showAddress(int x);

/* void showValue(int& x);
 * Permet d'afficher la valeur stockée à l'adresse passée en paramètre.
 * 
 * int& x : Adresse de la variable x.
 */
void showValue(int& x);

/* void swap(int* p1, int* p2);
 * Permet d'échanger le contenu des pointeurs p1 et p2.
 */
void swap(int* p1, int* p2);

/* void func(int tab[], int size, int** min, int** max);
 * Permet de stocker respectivement dans les pointeurs min et max les valeurs minimales et maximales du tableau tab.
 * 
 * int tab[] : Tableau d'entier.
 * int size : Taille du tableau.
 * int** min : Double pointeur sur entier.
 * int** max : Idem
 */
void func(int tab[], int size, int** min, int** max);

/* void fillArray(int* tab)
 * Permet de remplir 5 cases dans le tableau dont l'adresse de la première case est passée en paramètre.
 */
void fillArray(int* tab);

#endif